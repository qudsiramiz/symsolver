#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 2021

@author: Sevans
"""

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
   long_description = fh.read()

setup(
      name     = 'SymSolver',
      version  = '0.1',
      author   = 'Samuel Evans',
      author_email = 'sevans7@bu.edu',
      long_description=long_description,
      long_description_content_type='text/markdown',
      url      = 'https://gitlab.com/Sevans7/symsolver',
      packages = find_packages()
      )